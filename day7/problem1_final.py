class Directory:
    def __init__(self, name, depth, size, parent, children):
        self.name = name
        self.depth = depth
        self.size = size
        self.parent = parent
        self.children = children
    
    def root(self):
        self.name = '/'
        self.depth = 0
        self.parent = None
    
    def findChildren(self, contents_split):
        pass
        # for line in contents_split

        
def getCleanContents(contents) -> list:
    clean_contents = []
    for line in contents:
        line = line[:-1].split(" ")
        clean_contents.append(line)
    return clean_contents

def printList(ls):
    for element in ls:
        print(element)


# OPEN FILE
if input() == ' ':
    # Enter space for official
    FILE = "/home/alex_anast/workspace/advent_of_code_2022/day7/input"
else: # Just click enter for demo
    FILE = "/home/alex_anast/workspace/advent_of_code_2022/day7/input_0"
with open(FILE) as my_file:
    contents = my_file.readlines()


clean_contents = getCleanContents(contents)
printList(clean_contents)

if FILE[-1:] == '0':
    print("\n\ndemo")
else:
    print("\n\nofficial")